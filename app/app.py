from flask import Flask, render_template, request, redirect
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
db = SQLAlchemy(app)

class CarTiozao(db.Model):
    __tablename__= 'CarrosTio'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    carro = db.Column(db.String(300))

    def __init__(self, id=None, carro=None):
        self.id = id
        self.carro = carro

    def __repr__(self):
        return f'{self.carro}'


@app.route('/', methods=['GET'])
def index():
    return render_template('index.html', all=CarTiozao.query.all())

@app.route('/listing', methods=['POST'])
def listing():
    req = request.form['carro']
    db.session.add(CarTiozao(carro=req))
    db.session.commit()
    return redirect("/")

@app.route("/update", methods=["POST"])
def update():
    old_title = request.form['old_title']
    new_title = request.form['new_title']
    db.session.delete(CarTiozao.query.filter_by(carro=old_title).first())
    db.session.add(CarTiozao(carro=new_title))
    db.session.commit()
    return redirect("/")

@app.route("/delete", methods=["POST"])
def delete():
    delete_title = request.form['delete_title']
    db.session.delete(CarTiozao.query.filter_by(carro=delete_title).first())
    db.session.commit()
    return redirect("/")


if __name__ == '__main__':
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///carros.db'
    db.create_all()
    app.run(debug = True, host='0.0.0.0')
